"""
Command line args:
    --share: Общедоступныый локальный FLASK-сервер
"""
import sys
import socket

from werkzeug.serving import run_simple

from app import app


is_share_server = sys.argv[1] == '--share' if len(sys.argv) > 1 else False

if __name__ == '__main__':
    run_simple(
        sys.argv[2] or socket.gethostbyname(socket.gethostname()) if is_share_server else '127.0.0.1',
        5000, app, use_reloader=False
    )
