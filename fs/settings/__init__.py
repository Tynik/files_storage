import socket

from .main import *

# Override settings
if socket.gethostname() == PRODUCTION_HOST_NAME:
    from .production import *
else:
    from .local import *

__import__('fs.settings', fromlist=(
    'paths',
))
