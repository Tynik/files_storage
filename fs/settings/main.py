import os


BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(__file__)))

PRODUCTION_HOST_NAME = 'fs.ua'

DEBUG = False
TESTING = False

SECRET_KEY = '9D#2/)lw94Lk_+se3#LjdP90Dc56@sv'

APPS = (
    'fs'
)

SQLALCHEMY_TRACK_MODIFICATIONS = False

DB_DATE_TIME_FORMAT = '%Y-%m-%d %H:%M:%S'

UPLOAD_FILE_FOLDER = os.path.join(BASE_DIR, 'fs', 'uploads')
# Зависимость с моделью ``UploadFile``
ALLOWED_UPLOAD_FILE_EXT = ['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif']
