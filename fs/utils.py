import os
import typing

from datetime import datetime


def calc_obfuscated_file_name(name: str) -> typing.Tuple[str, str]:
    """
    Рассчитать обфусцированное имя файла

    Args:
        name (str): Имя файла

    Returns:
        typing.Tuple[str, str]: Обфусцированное имя файла в виде ``MD5`` хэша, расширение файла
    """
    import hashlib

    body, ext = os.path.splitext(name)

    md5 = hashlib.md5()
    # Хэширование имени файла
    md5.update(str('%s.%s' % (body, datetime.now().timestamp())).encode())
    return md5.hexdigest(), ext.lower()
