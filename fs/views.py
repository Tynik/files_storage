import os

from werkzeug.datastructures import MultiDict
from flask import (
    render_template,
    redirect,
    request,
    url_for,
    send_from_directory,
    flash,
)
from app import app

from . import (
    models,
    forms,
    utils
)


@app.route('/', methods=['GET'])
def index():
    """
    Главная страница сайта.

    Форма загрузки файла на сервер.
    Просмотр загруженный файлов.

    Notes:
        Только ``GET`` запрос
    """
    return render_template(
        'pages/index.html',
        uploaded_files=models.UploadFile.query.order_by(models.UploadFile.date_upload.desc()).all(),
        allowed_file_ext=app.config.get('ALLOWED_UPLOAD_FILE_EXT')
    )


@app.route('/upload', methods=['POST'])
def upload():
    """
    Загрузка файла на сервер.

    Notes:
        Только ``POST`` запрос
    """
    file = request.files['upload_name']
    # Получение типа файла и приведение его к нижнему регистру
    file_type = str(os.path.splitext(file.filename)[1][1:]).lower()
    # Форма валидации
    vf = forms.UploadFile(formdata=MultiDict(dict(
        name=file.filename,
        type=file_type
    )))
    # Валидация данных
    if vf.validate():
        _data = vf.data.get
        obs_fn = utils.calc_obfuscated_file_name(_data('name'))
        # Составление пути к файлу (Имя файла + расширение)
        dst_file = os.path.join(app.config['UPLOAD_FILE_FOLDER'], ''.join(obs_fn))
        # Сохранение файла на сервере
        file.save(dst_file)
        #
        # Добавление информации о файле в БД
        models.UploadFile.add(
            name=_data('name'),
            obfuscated_name=obs_fn[0],
            file_type=_data('type'),
        )
        # Передача сообщения о загрузке
        flash('Файл "%s" был успешно загружен на сервер' % _data('name'))
    else:
        # Передача ошибок
        flash(vf.errors, category='errors')

    return redirect('/')


@app.route('/get/<file_name>', methods=['GET'])
def receive_file(file_name):
    """
    Загрузка файла с сервера

    Args:
        file_name (str): Обфусцированнное имя файла

    Raises:
        NotFound: Файл не найден на сервере

    Notes:
        Только ``GET`` запрос
    """
    return send_from_directory(app.config['UPLOAD_FILE_FOLDER'], file_name)
