import wtforms

from app import app


class UploadFile(wtforms.Form):
    """
    Форма валидации для загрузки файла на сервер
    """
    # Имя файла
    name = wtforms.StringField(validators=[
        wtforms.validators.Length(min=1, max=255)
    ])
    # Тип файла.
    # Типы доступных файлов для загрузки заданы в файле конфигурации
    type = wtforms.SelectField(
        choices=[
            # id, name
            (e, '') for e in app.config.get('ALLOWED_UPLOAD_FILE_EXT', [])
        ],
        validators=[
            wtforms.validators.DataRequired(),
        ],
        coerce=str
    )
