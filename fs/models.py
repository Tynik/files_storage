import os
import typing
import datetime

from flask import url_for
from app import db, app

from . import utils


class UploadFile(db.Model):
    """
    Загруженный файл на сервер
    """
    __tablename__ = 'upload_file'

    id = db.Column(db.INTEGER, primary_key=True)
    # Имя файла
    name = db.Column(db.VARCHAR(255))
    # Хешированое имя файла
    obfuscated_name = db.Column(db.CHAR(32))
    # Размер
    size = db.Column(db.BIGINT, default=0)
    # Тип файла
    type = db.Column(db.Enum(*app.config.get('ALLOWED_UPLOAD_FILE_EXT', [])))
    # Дата загрузки
    date_upload = db.Column(db.DATETIME, default=lambda: datetime.datetime.now())

    def __init__(self, *args, **kwargs):
        super(UploadFile, self).__init__(*args, **kwargs)

        if not kwargs.get('obfuscated_name'):
            # Генерация обфусцированного имени файла
            self.obfuscated_name = utils.calc_obfuscated_file_name(kwargs.get('name'))[0]

        # Размер файла
        p2f = self.path2file
        if os.path.exists(p2f):
            self.size = os.path.getsize(p2f)

    def __repr__(self):
        return '<UploadFile %s>' % self.name

    @classmethod
    def add(cls, name: str, file_type: str, obfuscated_name: typing.Optional[str]=None) -> 'UploadFile':
        """
        Добавление информации о загруженном файле на сервер

        Args:
            name (str): Имя файла
            file_type (str): Тип файла
            obfuscated_name (str): Обфусцированное имя файла

        Returns:
            UploadFile: Экземпляр модели информации о загруженном файле
        """
        inst = cls(
            name=name,
            obfuscated_name=obfuscated_name,
            type=file_type
        )
        db.session.add(inst)
        db.session.commit()
        return inst

    @property
    def full_file_name(self) -> str:
        return '%s.%s' % (self.obfuscated_name, self.type)

    @property
    def path2file(self) -> str:
        return os.path.join(app.config['UPLOAD_FILE_FOLDER'], self.full_file_name)

    def link2file(self) -> str:
        return url_for('receive_file', file_name=self.full_file_name)

