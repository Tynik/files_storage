-- MySQL dump 10.13  Distrib 5.5.46, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: fs.ua
-- ------------------------------------------------------
-- Server version	5.7.9

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `fs.ua`
--

/*!40000 DROP DATABASE IF EXISTS `fs.ua`*/;

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `fs.ua` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;

USE `fs.ua`;

--
-- Table structure for table `upload_file`
--

DROP TABLE IF EXISTS `upload_file`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `upload_file` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `obfuscated_name` char(32) COLLATE utf8_unicode_ci NOT NULL,
  `size` bigint(20) NOT NULL,
  `type` enum('txt','pdf','png','jpg','jpeg','gif') COLLATE utf8_unicode_ci NOT NULL,
  `date_upload` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `obf_name` (`obfuscated_name`(4))
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `upload_file`
--

LOCK TABLES `upload_file` WRITE;
/*!40000 ALTER TABLE `upload_file` DISABLE KEYS */;
INSERT INTO `upload_file` (`id`, `name`, `obfuscated_name`, `size`, `type`, `date_upload`) VALUES (8,'79527452.jpg','3c4120a5fbed8b73fb8070074d78399c',240095,'jpg','2016-01-19 00:08:33'),(9,'3_1.jpg','2037119b9b6eafde64620bd8254b2f3d',10410195,'jpg','2016-01-19 00:12:39'),(10,'DSC_2327.jpg','bcfeddfcdc97ef5144ea4f5d3bb0b7a9',776841,'jpg','2016-01-19 00:14:01'),(11,'IMG_2214.jpg','b49327416ee8e0a36a42d1359cf32f03',455459,'jpg','2016-01-19 00:14:01'),(12,'_DSC3066.jpg','1a1322c8b9eb5dc73c0ab9550107513f',8485062,'jpg','2016-01-19 00:14:01'),(13,'Internet_of_Things_IoT_IBSG_0411FINAL.pdf','c3eb1fcc89e7d6a48b713b58c277d0f5',775082,'pdf','2016-01-19 00:14:01'),(14,'Python.Flask.1.0.pdf','61485cf9103f6c44687c5d0a4dfb76c4',1274829,'pdf','2016-01-19 00:14:01'),(15,'The Rails View.pdf','8fd0ab58ee4cac3bc0c8f94c93048521',8915008,'pdf','2016-01-19 00:17:03'),(16,'VE2xD41NOK0.jpg','b65276e878766a65a5c64e431c68dee6',724471,'jpg','2016-01-19 00:17:42'),(17,'DSC_2777.JPG','b97843fdd2c4a17975097426bd4a9aaf',0,'jpg','2016-01-19 00:44:46'),(18,'VE2xD41NOK0.jpg','e811bed98436a73c00e38e251de13f17',724471,'jpg','2016-01-19 00:45:23'),(19,'_DSC3025.jpg','ca8c246af6064c3a7f9bb3582e8cb27f',5806678,'jpg','2016-01-19 00:45:34'),(20,'IMG_2210-s.jpg','5d5f1423e109184f7f47496d62af051f',470809,'jpg','2016-01-19 00:45:52');
/*!40000 ALTER TABLE `upload_file` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-01-19  0:48:08