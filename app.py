import os

from flask import Flask
from flask.ext.sqlalchemy import SQLAlchemy


app = Flask('fs')
# Set configuration
app.config.from_object('fs.settings')
# Init Db
db = SQLAlchemy(app)

__import__('fs', fromlist=(
    'middlewares',
    'views',
))
